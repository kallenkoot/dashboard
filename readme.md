# Repository clonen

`git clone git@gitlab.com:kallenkoot/dashboard.git`


# Dependencies installeren
```bash
composer install
npm update
npm run dev
```

# App configureren
`cp .env.example .env`

### Database aanmaken
`touch database/database.sqlite`

### Genereer een app key

`php artisan key:generate`

### Database, Pusher, user info configuratie

```
DB_CONNECTION=sqlite

BASIC_AUTH_USERNAME=test@test.nl
BASIC_AUTH_PASSWORD=wachtwoord

PUSHER_APP_ID=APP
PUSHER_KEY=
PUSHER_SECRET=
PUSHER_CLUSTER=
```

`php artisan migrate --seed`

## Webserver

`php artisan serve`

`php artisan schedule:run`

## Frontend assets

`npm run dev`

of 

`npm run watch`

om het bij file-save te draaien

